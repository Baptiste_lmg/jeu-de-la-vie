from tkinter import *
from classmortvivant import *


root = Tk()
root.title("JeudelavieBaptiste")
frame = Frame(root, width=800, height=800)
frame.pack()
canvas = Canvas(frame, width=500, height=500)
canvas.pack()


def tableau():
    x = 10
    y = 10
    global g 
    global rec 
    rec = []
    g = []
    for i in range(70):
        g.append([])
        rec.append([])
        for j in range(70):
            rect = canvas.create_rectangle(x, y, x+10, y+10, fill="white")
            rec[i].append(rect)
            g[i].append(Cell(x, y, i, j))
            x += 10
        x = 10
        y += 10


def clik_xy(x, y):

    return (int(x- x%10), int(y - y%10))


def whi_to_blk(event):

    x, y = clik_xy(event.x, event.y)
    try:
        iy =int( x / 10 ) - 1
        ix = int(y / 10) - 1
        if ix == -1 or iy == -1:
            raise IndexError
        if g[ix][iy].isAlive:
            canvas.itemconfig(rec[ix][iy], fill="white")
        else:
            canvas.itemconfig(rec[ix][iy], fill="black")
        g[ix][iy].switchStatus()
    except IndexError:
        return


def paint_grid():
    for i in g:
        for j in i:
            if j.nextStatus != j.isAlive:
                x, y = j.pos_matrix
                if j.nextStatus:
                    canvas.itemconfig(rec[x][y], fill="black")
                else:
                    canvas.itemconfig(rec[x][y], fill="white")
                j.switchStatus()


def changeInStatus(cell):
    num_alive = 0
    x, y = cell.pos_matrix
    for i in (x-1, x, x+1):
        for j in (y-1, y, y+1):
            if i == x and j == y:
                continue
            if i == -1 or j == -1:
                continue
            try:
                if g[i][j].isAlive:
                    num_alive += 1
            except IndexError:
                pass
    if cell.isAlive:
        return not( num_alive == 2 or num_alive == 3 )
    else:
        return num_alive == 3


def gogame():
    for i in g:
        for j in i:
            if changeInStatus(j):
                j.nextStatus = not j.isAlive
            else:
                j.nextStatus = j.isAlive
    paint_grid()
    global begin_id
    begin_id = root.after(200, gogame)


def arretgame():
    root.after_cancel(begin_id)

tableau()
start = Button(root, text="Marche ", command=gogame)
start.pack(side = LEFT)
stop = Button(root, text="Arrete", command = arretgame)
stop.pack(side = RIGHT)
canvas.bind("<Button-1>", whi_to_blk)
root.mainloop()